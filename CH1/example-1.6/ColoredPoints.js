/**
 * Created by yangyanjun on 15/6/12.
 */

// 顶点着色器
var VSHADER_SOURCE =
    'attribute vec4 a_Position;\n' +
    'void main() {\n' +
    '  gl_Position = a_Position;\n' +
    '  gl_PointSize = 10.0;\n' +
    '}\n';

// 片段着色器
var  FSHADER_SOURCE =
    'precision mediump float;\n' +
    'uniform vec4 u_FragColor;\n' +  // uniform变量
    'void main() {\n' +
    '  gl_FragColor = u_FragColor;\n' +
    '}\n';

function main() {
    var canvas = document.getElementById('webgl');

    var gl = getWebGLContext(canvas);
    if (!gl) {
        console.log('获取WebGL失败');
        return;
    }

    // 初始化着色器
    if (!initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE)) {
        console.log('初始化着色器失败');
        return;
    }

    // 获取a_Position变量的存储位置
    var a_Position = gl.getAttribLocation(gl.program, 'a_Position');
    if (a_Position < 0) {
        console.log('获取位置 a_Position 失败');
        return;
    }

    // 获取u_FragColor变量的存储位置
    var u_FragColor = gl.getUniformLocation(gl.program, 'u_FragColor');
    if (!u_FragColor) {
        console.log('获取颜色 u_FragColor 失败');
        return;
    }

    // 注册鼠标点击事件响应函数
    canvas.onmousedown = function(ev) {
        click(ev, gl, canvas, a_Position, u_FragColor);
    };

    // 设置<canvas>颜色
    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    // 清除<canvas>
    gl.clear(gl.COLOR_BUFFER_BIT);
}

var g_points = []; // 鼠标点击位置数组
var g_colors = []; // 存储点颜色的数组
function click(ev, gl, canvas, a_Position, u_FragColor) {
    var x = ev.clientX;
    var y = ev.clientY;
    var rect = ev.target.getBoundingClientRect();

    x = ((x - rect.left) - canvas.width/2)/(canvas.width/2);
    y = (canvas.height/2 - (y - rect.top))/(canvas.height/2);

    // 将坐标存入g_points数组中
    g_points.push([x, y]);

    // 将颜色存入g_colors数组中
    if (x >= 0.0 && y >= 0.0) {      // 第一象限
        g_colors.push([1.0, 0.0, 0.0, 1.0]);  // Red
    }else if (x < 0.0 && y >= 0.0) { // 第二象限
        g_colors.push([0.0, 1.0, 0.0, 1.0]);  // Green
    }else if (x < 0.0 && y < 0.0) { // 第三象限
        g_colors.push([0.0, 0.0, 1.0, 1.0]);  // Green
    } else {                         // 其他(第四象限)
        g_colors.push([1.0, 1.0, 1.0, 1.0]);  // White
    }

    // 清除<canvas>
    gl.clear(gl.COLOR_BUFFER_BIT);

    var len = g_points.length;
    for(var i = 0; i < len; i += 2) {
        var xy = g_points[i];
        var rgba = g_colors[i];

        // 将点的位置传递到变量 a_Position 中
        gl.vertexAttrib3f(a_Position, xy[0], xy[1], 0.0);
        // 将点的颜色传递到变量 u_FragColor 中
        gl.uniform4f(u_FragColor, rgba[0], rgba[1], rgba[2], rgba[3]);
        // 绘制点
        gl.drawArrays(gl.POINTS, 0, 1);
    }
}