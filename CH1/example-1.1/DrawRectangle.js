/**
 * Created by yangyanjun on 15/6/12.
 */

function main() {

    // 获取<canvas>元素
    var canvas = document.getElementById('canvas');
    if (!canvas) {
        console.log('canvas初始化失败');
        return;
    }

    // 获取绘制二维图形的绘图上下文
    var ctx = canvas.getContext('2d');

    // 绘制一个蓝色矩形
    ctx.fillStyle = 'rgba(0, 0, 255, 1.0)';
    ctx.fillRect(120, 10, 150, 150);
}