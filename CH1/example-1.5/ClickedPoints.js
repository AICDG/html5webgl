/**
 * Created by yangyanjun on 15/6/12.
 */

// 顶点着色器
var VSHADER_SOURCE =
    'attribute vec4 a_Position;\n' +
    'void main() {\n' +
    '  gl_Position = a_Position;\n' +
    '  gl_PointSize = 10.0;\n' +
    '}\n';

// 片段着色器
var FSHADER_SOURCE =
    'void main() {\n' +
    '  gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);\n' +
    '}\n';

function main() {
    var canvas = document.getElementById('webgl');

    var gl = getWebGLContext(canvas);
    if (!gl) {
        console.log('获取WebGL失败');
        return;
    }

    // 初始化着色器
    if (!initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE)) {
        console.log('初始化着色器失败');
        return;
    }

    // 获取attribute变量的存储位置
    var a_Position = gl.getAttribLocation(gl.program, 'a_Position');
    if (a_Position < 0) {
        console.log('获取位置 a_Position 失败');
        return;
    }

    // 注册鼠标点击事件响应函数
    canvas.onmousedown = function(ev) {
        click(ev, gl, canvas, a_Position);
    };

    // 设置<canvas>颜色
    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    // 清除<canvas>
    gl.clear(gl.COLOR_BUFFER_BIT);
}

var g_points = []; // 鼠标点击位置数组
function click(ev, gl, canvas, a_Position) {
    var x = ev.clientX;
    var y = ev.clientY;
    var rect = ev.target.getBoundingClientRect();

    x = ((x - rect.left) - canvas.width/2)/(canvas.width/2);
    y = (canvas.height/2 - (y - rect.top))/(canvas.height/2);

    // 将坐标存入g_points数组中
    g_points.push(x);
    g_points.push(y);

    // 清除<canvas>
    gl.clear(gl.COLOR_BUFFER_BIT);

    var len = g_points.length;
    for(var i = 0; i < len; i += 2) {
        // 将点的位置传递到变量 a_Position 中
        gl.vertexAttrib3f(a_Position, g_points[i], g_points[i+1], 0.0);

        // 绘制点
        gl.drawArrays(gl.POINTS, 0, 1);
    }
}