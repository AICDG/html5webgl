/**
 * Created by yangyanjun on 15/6/12.
 */

function main() {
    var canvas = document.getElementById('webgl');

    var gl = getWebGLContext(canvas);

    // 获取webGL绘图上下文
    if (!gl) {
        console.log('不能获取webGL');
        return;
    }

    // 指定清空<canvas>的颜色
    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    // 清空<canvas>
    gl.clear(gl.COLOR_BUFFER_BIT);
}